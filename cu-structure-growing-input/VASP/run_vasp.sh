#!/bin/bash

#SBATCH -p MMM
#SBATCH -J vasp               # Job name
# #SBATCH -o vasp.%j.out        # Name of stdout output file (%j expands to jobId)
# #SBATCH -e vasp.%j.err        # Name of stdout output file (%j expands to jobId)
#SBATCH -N 1                  # Total number of nodes requested
#SBATCH -n 16                 # Total number of mpi tasks requested
#SBATCH -t 3:00:00           # Run time (hh:mm:ss) - 12 hours
# #SBATCH --mem-per-cpu=7675
#SBATCH --exclusive
# #SBATCH -x node-mmm21,node-mmm22,node-mmm20,node-mmm19,node-mmm18,node-mmm17

# Launch MPI-based executable
hostname
hostname >./$SLURM_JOB_NAME'.'$SLURM_JOB_ID
date
date >>./$SLURM_JOB_NAME'.'$SLURM_JOB_ID

export OMP_NUM_THREADS=1
module load Compiler/Intel/16u4 Q-Ch/VASP/5.4.4

mpirun -n $SLURM_NTASKS vasp_gam >>./$SLURM_JOB_NAME'.'$SLURM_JOB_ID

rm CHGCAR CHG WAVECAR

echo "1" >> is_vasp_finished.txt
date >>./$SLURM_JOB_NAME'.'$SLURM_JOB_ID
date
