#!/bin/bash
export OMP_NUM_THREADS=1
src=$(pwd)
cd $src
echo "lammps MD is starting"
mpirun -n 16 ./lmp_mpi -in in.my
echo "lampps MD completed"
echo "1" >> is_lammps_finished.txt
