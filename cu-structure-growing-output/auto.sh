#!/bin/bash

cp init.almtp curr.almtp
cp train_one.cfg train.cfg
cp Cu_1620.data dump.trj
is_training_finished=0
sbatch -J pre_train -o pre_train.out -e pre_train.err  -N 1 --exclusive -p high,MMM ./pre_train.sh --time=3:00:00
while [ $is_training_finished -ne 1 ]
do
    sleep 5
    if [ -f is_training_finished.txt ]; then
        is_training_finished=1
        echo $is_training_finished
    else
        is_training_finished=0
        echo $is_training_finished
    fi
done
sleep 5
rm -f *.txt
for p in $(seq 1621 3000)
do
	echo "# LAMMPS data file written by OVITO Basic 3.7.11" > Cu_${p}.data
        echo "${p} atoms" >> Cu_${p}.data
	prev=$((${p}-1))
	cat Cu_${prev}.data | tail -n +3 | head -n 11 >> Cu_${p}.data
	cat dump.trj | tail -n ${prev} >> Cu_${p}.data
	python coord.py $p
	echo "active learning with ${p} atoms is starting"
	./md_al_mtp.sh
	cp dump.trj pot_save/
        cp train.cfg pot_save/
        cd pot_save
        mv dump.trj dump_${p}.trj
        mv train.cfg train_${p}.cfg
        cp curr.almtp curr_${p}.almtp
        cd ../
        cp pot_save/curr.almtp .
done 
